﻿using System.IO;
using MSCLoader;
using UnityEngine;

namespace WorkingApartmentFlagReplacer
{
    public class WorkingApartmentFlagReplacer : Mod
    {
        public override string ID => "WorkingApartmentFlagReplacer";
        public override string Version => "1.0";
        public override string Author => "アカツキ";

        public override bool UseAssetsFolder => true;

        public override void ModSetup()
        {
            base.ModSetup();

            SetupFunction(Setup.PostLoad, On_Load);
        }

        private void On_Load()
        {
            if (!File.Exists(Path.Combine(ModLoader.GetModAssetsFolder(this), "Flag.png")))
            {
                ModConsole.Log("Please put replacement flag in " + Path.Combine(ModLoader.GetModAssetsFolder(this), "Flag.png"));
                return;
            }

            var customFlag = LoadAssets.LoadTexture(this, "Flag.png");

            GameObject.Find("Garage(Clone)").transform.GetChild(5).gameObject
                .GetComponent<Renderer>().sharedMaterial = new Material(Shader.Find("Standard"))
            {
                mainTexture = customFlag
            };
        }
    }
}